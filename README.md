# web-crypto-lite

convenient client-side symmetric encryption/decryption wrapper

## License

MIT

## Installation

```bash
npm i web-crypto-lite
```

## Usage

```javascript
import { Crypto } from "web-crypto-lite";

async function demo() {
    const password = "mySecR3T";
    const data = { msg: "for your eyes only" }
    
    const cipher = await Crypto.encrypt( data, password );

    const received = await Crypto.decrypt( cipher, password );
	
    console.log( received.msg === data.msg );     // true
}
```
