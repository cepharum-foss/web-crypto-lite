export = WebCryptoLite;

declare module WebCryptoLite {
    class Crypto {
        /**
         * Transcodes URL-safe hash into base64-encoded data.
         *
         * @param hash hash optimized for safe embedding in a URL
         */
        static hashToBase64(hash: string): string;

        /**
         * Transcodes base64-encoded data into URL-safe hash.
         *
         * @param base64 base64-encoded code to transform
         */
        static base64ToHash(base64: string): string;

        /**
         * Base64-encodes provided string based on UTF8 character encoding.
         *
         * @note This converter takes a regular string usually containing UCS2
         *       characters. It is converted to UTF8 encoding before encoding
         *       with Base64.
         *
         * @param text regular text to be encoded
         */
        static textToBase64(text: string): string;

        /**
         * Decodes base64-encoded data into string based on UTF8 character
         * encoding.
         *
         * @note This converter decodes a Base64-encoded string into octets
         *       assumed to represent text in UTF8 encoded. Afterwards, this
         *       UTF8-encoded data is converted into regular UCS2 string.
         *
         * @param base64 base64-encoded data
         */
        static base64ToText(base64: string): string;

        /**
         * Base64-encodes sequence of octets.
         *
         * @param buffer octets to encode
         */
        static bufferToBase64(buffer: Uint8Array): string;

        /**
         * Base64-decodes sequence of octets.
         *
         * @param base64 base64-encoded binary data
         */
        static base64ToBuffer(base64: string): Uint8Array;

        /**
         * Transcodes regular string into string with each character
         * representing another octet of that text in UTF8 encoding.
         *
         * @param text regular UCS2-encoded string
         */
        static textToUtf8(text: string): string;

        /**
         * Transcodes string of characters each representing another octet in a
         * UTF8-encoded text into regular UCS2-encoded string with same text.
         *
         * @param utf8 string of characters each representing octet of UTF8-encoding
         */
        static utf8ToText(utf8: string): string;

        /**
         * Derives from provided password input for creating actual key suitable
         * for encrypting/decrypting data symmetrically.
         *
         * @param password clear-text password e.g. as provided by a user
         */
        static getKeyMaterial(password: string): Promise<CryptoKey>;

        /**
         * Derives from provided password key suitable for encrypting/decrypting
         * data symmetrically.
         *
         * @param password clear-text password e.g. as provided by a user
         * @param salt sequence of random octets for salting key derivation
         */
        static getKey(password: string, salt: Uint8Array): Promise<CryptoKey>;

        /**
         * Symmetrically encrypts provided data using key internally derived
         * from given password.
         *
         * @param data data to be encrypted, will be JSON-encoded and UTF8-encoded
         * @param password encryption/decryption password, e.g. as provided by a user
         */
        static encrypt( data: any, password: string ): Promise<string>;

        /**
         * Symmetrically decrypts data from provided cipher delivered by
         * encrypt() before using key internally derived from given password.
         *
         * @param cipher cipher as returned by encrypt()
         * @param password encryption/decryption password, e.g. as provided by a user
         */
        static decrypt( cipher: string, password: string ): Promise<any>;
    }
}
