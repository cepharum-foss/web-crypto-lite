/**
 * (c) 2021 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

/**
 * Implements helpers for encrypting/decrypting data client-side.
 */
export class Crypto {
	/**
	 * Transcodes URL-encodable hash into base64-encoded data.
	 *
	 * @param {string} hash hash optimized for embedding in a URL
	 * @returns {string} base64-encoded data represented by provided hash
	 */
	static hashToBase64( hash ) {
		const code = hash.replace( /-/g, "+" ).replace( /_/g, "/" );

		return code + "===".slice( 0, ( 4 - ( code.length % 4 ) ) % 4 );
	}

	/**
	 * Transcodes base64-encoded data into hash suitable for use in a URL w/o
	 * extra encoding.
	 *
	 * @param {string} code base64-encoded code to transform
	 * @returns {string} hash representing same code
	 */
	static base64ToHash( code ) {
		return code.replace( /\+/g, "-" ).replace( /\//g, "_" ).replace( /=+$/, "" );
	}

	/**
	 * Encodes provided string with base64.
	 *
	 * @param {string} string string to be encoded
	 * @returns {string} string recovered from base64 encoding
	 */
	static textToBase64( string ) {
		return btoa( this.textToUtf8( string ) );
	}

	/**
	 * Recovers base64-encoded string.
	 *
	 * @param {string} data base64-encoded sting to recover
	 * @returns {string} string recovered from base64 encoding
	 */
	static base64ToText( data ) {
		return this.utf8ToText( atob( data ) );
	}

	/**
	 * Converts sequence of octets into binary base64 encoded string.
	 *
	 * @param {Uint8Array} buffer sequence of octets to encode
	 * @returns {string} base64-encoded sequence of octets
	 */
	static bufferToBase64( buffer ) {
		const numOctets = buffer.length;
		let string = "";

		for ( let i = 0; i < numOctets; i++ ) {
			string += String.fromCharCode( buffer[i] );
		}

		return btoa( string );
	}

	/**
	 * Decodes base64-encoded sequence of octets.
	 *
	 * @param {string} encoded base64-encoded sequence of octets
	 * @returns {Uint8Array} decoded sequence of octets
	 */
	static base64ToBuffer( encoded ) {
		const numOctets = encoded.length;
		const buffer = new Uint8Array( numOctets );

		for ( let i = 0; i < numOctets; i++ ) {
			buffer[i] = encoded.charCodeAt( i );
		}

		return buffer;
	}

	/**
	 * Converts regular string into string with each character representing
	 * another octet of that text in UTF8 encoding.
	 *
	 * @param {string} string UCS2 string using two octets per character
	 * @returns {string} UCS2 string with each character representing another octet of UTF8-encoded string
	 */
	static textToUtf8( string ) {
		const charCodes = new TextEncoder().encode( string );
		let result = "";
		for ( let i = 0; i < charCodes.byteLength; i++ ) {
			result += String.fromCharCode( charCodes[i] );
		}
		return result;
	}

	/**
	 * Converts binary string into regular string.
	 *
	 * @param {string} utf8 UCS2 string with each character representing octet of UTF8-encoded string
	 * @returns {string} UCS2 string recompiled from provided binary string
	 */
	static utf8ToText( utf8 ) {
		const bytes = new Uint8Array( utf8.length );
		for ( let i = 0; i < bytes.length; i++ ) {
			bytes[i] = utf8.charCodeAt( i );
		}

		return new TextDecoder().decode( bytes );
	}

	/**
	 * Derives input for generating key suitable for encrypting/decrypting.
	 *
	 * @param {string} password some custom password
	 * @returns {Promise<CryptoKey>} promises key bits derived from provided password
	 * @protected
	 */
	static async getKeyMaterial( password ) {
		return await window.crypto.subtle.importKey(
			"raw",
			new TextEncoder().encode( password ),
			{ name: "PBKDF2" },
			false,
			[ "deriveBits", "deriveKey" ]
		);
	}

	/**
	 * Sets up key suitable for encrypting/decrypting using AES.
	 *
	 * @param {string} password custom password
	 * @param {Uint8Array} salt salt to use on deriving key from password
	 * @param {string} algorithm name of encryption algorithm key will be used with (either "AES-GCM" or "AES-CBC")
	 * @returns {Promise<CryptoKey>} promises key suitable for encrypting/decrypting
	 */
	static async getKey( password, salt, algorithm = "AES-GCM" ) {
		const keyMaterial = await this.getKeyMaterial( password );

		return window.crypto.subtle.deriveKey( {
				name: "PBKDF2",
				salt,
				iterations: 100000,
				hash: "SHA-256",
			},
			keyMaterial,
			{ name: algorithm, length: 256 },
			true,
			[ "encrypt", "decrypt" ] );
	}

	/**
	 * Encrypts provided data with given password.
	 *
	 * @param {*} data data to be encrypted
	 * @param {string} password password for encrypting
	 * @returns {Promise<string>} promises encrypted data
	 */
	static async encrypt( data, password ) {
		const cleartext = new TextEncoder().encode( JSON.stringify( data ) );
		const salt = window.crypto.getRandomValues( new Uint8Array( 16 ) );

		let cipher;
		let name = "AES-GCM";
		let iv = window.crypto.getRandomValues( new Uint8Array( 12 ) );
		let key = await this.getKey( password, salt, name );

		console.log( iv );
		try {
			cipher = await window.crypto.subtle.encrypt( { name, iv }, key, cleartext );
		} catch ( c ) {
			name = "AES-CBC";
			iv = window.crypto.getRandomValues( new Uint8Array( 16 ) );
			key = await this.getKey( password, salt, name );

			cipher = await window.crypto.subtle.encrypt( { name, iv }, key, cleartext );
		}

		return [ name, this.bufferToBase64( salt ), this.bufferToBase64( iv ), this.bufferToBase64( cipher ) ].join( ":" );
	}

	/**
	 * Decrypts provided data with given password.
	 *
	 * @param {string} cipher encrypted data
	 * @param {string} password password for decrypting (same as for encrypting)
	 * @returns {Promise<*>} promises decrypted data
	 */
	static async decrypt( cipher, password ) {
		const parts = String( cipher ).replace( /\s+/g, "" ).split( ":" );
		if ( parts.length !== 4 ) {
			throw new TypeError( "provided cipher does not contain required constituents" );
		}

		const name = parts[0];
		const salt = this.base64ToBuffer( parts[1] );
		const iv = this.base64ToBuffer( parts[2] );
		const encrypted = this.base64ToBuffer( parts[3] );

		const key = await this.getKey( password, salt, name );
		const cleartext = await window.crypto.subtle.decrypt( { name, iv }, key, encrypted );

		return JSON.parse( new TextDecoder().decode( cleartext ) );
	}
}
