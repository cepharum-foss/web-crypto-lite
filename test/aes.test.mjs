import { crypto } from "https://deno.land/std@0.116.0/crypto/mod.ts";
import "../node_modules/should/should.js";

import {Crypto} from "../index.mjs";

Deno.test( "AES symmetric encryption properly encrypts and decrypts an object", async () => {
	const token = "myS3cRet🠶";
	const original = {
		msg: "🠶 for your eyes, only!"
	};

	const cipher = await Crypto.encrypt( original, token );

	console.log( cipher );

	const copy = await Crypto.decrypt( cipher, token );

	copy.should.be.Object().which.has.ownProperty( "msg" ).which.is.equal( original.msg );
} );
