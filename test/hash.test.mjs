import "../node_modules/should/should.js";

import { Crypto } from "../index.mjs";

Deno.test( "Base64-encoded strings can be converted to/from URL-safe hash", () => {
	Crypto.base64ToHash( "aA/+aA==" ).should.equal( "aA_-aA" );
	Crypto.hashToBase64( "aA_-aA" ).should.equal( "aA/+aA==" );
} );
