import "../node_modules/should/should.js";

import { Crypto } from "../index.mjs";

Deno.test( "UTF-8 transcoding properly handles single-octet codepoints", () => {
	Crypto.utf8ToText( Crypto.textToUtf8( "Hello World!" ) ).should.equal( "Hello World!" );
} );

Deno.test( "UTF-8 transcoding properly handles multi-octet codepoints", () => {
	Crypto.utf8ToText( Crypto.textToUtf8( "René, der Gärtner" ) ).should.equal( "René, der Gärtner" );
} );

Deno.test( "UTF-8 transcoding properly handles supplementals", () => {
	Crypto.textToUtf8( "🠶" ).should.have.length( 4 );
	Crypto.utf8ToText( Crypto.textToUtf8( "🠶" ) ).should.equal( "🠶" );
} );
